 // app/routes.js

// grab the user model we just created
var User            = require('./models/user');
var Presentation    = require('./models/presentation');
var Item            = require('./models/item');
var express         = require('express');
var app             = express();
var fs              = require('fs');
var fx              = require('fs-extra');
var hb              = require('handlebars');
var zip             = require('easy-zip');
var flow = require('./flow-node.js')('tmpUpload');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

    module.exports = function(app) {






// Configure access control allow origin header stuff
var ACCESS_CONTROLL_ALLOW_ORIGIN = false;

// Host most stuff in the public folder
app.use(express.static('../public/img'));

// Handle uploads through Flow.js
app.post('/uploads', multipartMiddleware, function(req, res) {
  flow.post(req, function(status, filename, original_filename, identifier) {
    console.log('POST', status, original_filename, identifier);
    var stream = fs.createWriteStream(filename);
    flow.write(identifier, stream);
    fx.move(filename, 'public/img/'+filename, function (err) {
        if (err) return console.error(err);

    });
    if (ACCESS_CONTROLL_ALLOW_ORIGIN) {
      res.header("Access-Control-Allow-Origin", "*");
    }
    res.status(status).send();
  });
});


app.options('/uploads', function(req, res){
  console.log('OPTIONS');
  if (ACCESS_CONTROLL_ALLOW_ORIGIN) {
    res.header("Access-Control-Allow-Origin", "*");
  }
  res.status(200).send();
});

// Handle status checks on chunks through Flow.js
app.get('/uploads', function(req, res) {
  flow.get(req, function(status, filename, original_filename, identifier) {
    console.log('GET', status);
    if (ACCESS_CONTROLL_ALLOW_ORIGIN) {
      res.header("Access-Control-Allow-Origin", "*");
    }

    if (status == 'found') {
      status = 200;
    } else {
      status = 204;
    }

    res.status(status).send();
  });
});











        // server routes ===========================================================
        // handle things like api calls
        // authentication routes


       // user routes ===========================================================
        // sample api route

        app.get('/api/users/:user_id', function(req, res) {
            User.find({
                _id : req.params.user_id
            }, function(err, user) {
                if (err)
                    res.send(err);
                // get and return the user 
            
                res.json(user);
            });
        });

        app.get('/api/users', function(req, res) {
            // use mongoose to get all users in the database
            User.find(function(err, users) {
                // if there is an error retrieving, send the error. 
                                // nothing after res.send(err) will execute
                if (err)
                    res.send(err);

                res.json(users); // return all users in JSON format
            });
        });

        // route to handle creating goes here (app.post)
        app.post('/api/users', function(req, res) {

            // create a user, information comes from AJAX request from Angular
            User.create({
                firstname : req.body.firstname,
                lastname : req.body.lastname,
                email : req.body.email,
                password : req.body.password,
                presentations : []
            }, function(err, user) {
                if (err)
                    res.send(err);

                // get and return all the users after you create another
                User.find(function(err, users) {
                    if (err)
                        res.send(err)
                    res.json(users);
                });
            });

        });
//UPDATE
        app.put('/api/users/:user_id', function(req, res, next) {
            User.findByIdAndUpdate(req.params.user_id, req.body, function(err, post){
                if(err) return next(err);
                res.json(post);
            });
        });
        // route to handle delete goes here (app.delete)
        app.delete('/api/users/:user_id', function(req, res) {
            User.remove({
                _id : req.params.user_id
            }, function(err, user) {
                if (err)
                    res.send(err);

                // get and return all the users after you create another
                User.find(function(err, users) {
                    if (err)
                        res.send(err)
                    res.json(users);
                });
            });
        });


        // presentation routes ===========================================================
 
         app.get('/api/presentations', function(req, res) {
            // use mongoose to get all presentations in the database
            Presentation.find(function(err, presentations) {
                // if there is an error retrieving, send the error. 
                // nothing after res.send(err) will execute
                if (err)
                    res.send(err);

                res.json(presentations); // return all presentations in JSON format
            });
        });

        // route to handle get goes here (app.get)
        app.get('/api/presentations/:presentation_id', function(req, res) {
            Presentation.find({
                _id : req.params.presentation_id
            }, function(err, presentation) {
                if (err)
                    res.send(err);
                // get and return the presentation 
                res.json(presentation);
            });
        });



    //------------------------------EXPORT------------------------------------

        // route to handle get goes here (app.get)
        app.get('/api/presentations/:presentation_id/export', function(req, res) {
            Presentation.find({
                _id : req.params.presentation_id
            }, function(err, presentation) {
                if (err)
                    res.send(err);
            

                                var el = "{{#elements}}\t\t\t\t\t\t<div class=\"elementwrapper\">\n\t\t\t\t\t\t\t<div id=\"{{id}}\" class=\"element {{type}} {{#each ../../../css}}{{Alignment}} {{Fontalign}} {{Fragments}}{{/each}}\" ng-class=\"{elementInactive:{{id}}>=activeElement&&activeSubSection=={{../id}}&&'{{type}}'=='fragment',elementActive:activeElement>{{id}}&&{{../../id}}==activeSection||'{{type}}'!='fragment'&&{{../../id}}==activeSection}\" style=\"{{#each properties}}{{@key}}:{{this}};{{/each}}\">{{{content}}}</div>\n\t\t\t\t\t\t</div>\n{{/elements}}";
                            var sub = "{{#subsections}}\t\t\t\t\t<div id=\"{{id}}\" class=\"subsection {{#each ../../css}}{{Theme}} {{Effect}} {{Background}} {{Colors}}{{/each}}\" ng-class=\"{active:activeSubSection=={{id}}&&activeSection=={{../id}}, subSectionTop:{{../id}}==activeSection&&{{id}}==activeSubSection-1||{{../id}}==activeSection-1&&{{id}}<=activeSubSection, subSectionBottom:{{../id}}==activeSection&&{{id}}==activeSubSection+1||{{../id}}==activeSection+1&&{{id}}>0}\" ng-click=\"chooseSubSection({{../id}},{{id}})\">\n"+el+"\n\t\t\t\t\t</div>\n{{/subsections}}";
                        var sec = "{{#sections}}\t\t\t\t<div id=\"{{id}}\" ng-class=\"{section:true, sectionLeft:{{id}}==activeSection-1, sectionRight:{{id}}==activeSection+1, sectionLeftInactive:{{id}}<activeSection-1, sectionRightInactive:{{id}}>activeSection+1}\" class=\"{{#each ../css}}{{Effect}}{{/each}}\">\n"+sub+"\t\t\t\t</div>\n{{/sections}}";

                    var wholepresentation = "\t\t\t<div class=\"{{#css}}{{Font}}{{/css}}\">\n\n"+sec+"\n\t\t\t</div>\n";
                var body = "\t<body>\n\t\t<div class=\"preview\" ng-controller=\"PrevCtrl\">\n\n"+wholepresentation+"\n\t\t</div>\n\n\n\t\t<div class=\"overview\" ng-if=\"view=='overview'\">\n\n"+wholepresentation+"\n\n\t\t</div>\n\n\n\t</body>\n";
                    var javascript = "\t\t<script src=\"libs/angular.js\"></script>\n";
                        javascript += "\t\t<script src=\"js/app.js\"></script>\n";
                    var styles = "\t\t<link rel=\"stylesheet\" href=\"css/preview.css\">\n";
                        styles += "\t\t<link rel=\"stylesheet\" href=\"css/preset/designs.css\">\n";
                        styles += "\t\t<link rel=\"stylesheet\" href=\"css/preset/fonts.css\">\n";
                        styles += "\t\t<link rel=\"stylesheet\" href=\"css/preset/colors.css\">\n";
                    var title = "\n\t\t<meta charset=\"UTF-8\">";
                        title += "\n\t\t<title>"+presentation[0].title+"</title>\n";
                var head = "\t<head>\n"+title+"\n"+styles+"\n"+javascript+"\n\t</head>";
            var source = "<html ng-app=\"PresentationApp\">\n"+head+"\n"+body+"</html>";

            var template = hb.compile(source);
             
            var json = presentation[0];
            var result = template(json);

            var exportDest = 'tmp/index.html';

            fs.writeFile(exportDest, result, function(err){
                if(err) res.send(err);
                console.log('Index-File was created');

                fx.copy('public/css/preview.css', 'tmp/css/preview.css',{replace: true}, function (err) {
                    if (err) return console.error(err);

                    fx.copy('public/css/preset', 'tmp/css/preset', function (err) {
                      if (err) return console.error(err);

                        fx.copy('public/img', 'tmp/img', function (err) {
                          if (err) return console.error(err);
                          console.log('CSS-Files were copyied');
                        });

                    });

                });
                fx.copy('public/libs/angular/angular.js', 'tmp/libs/angular.js',{replace:true}, function (err) {
                    if (err) return console.error(err);
                      console.log('Libraries were copyied');
                });

            });


            fs.writeFile('tmp/.json', JSON.stringify(presentation[0]), function(err){
                if(err) res.send(err);
                console.log('JSON-File was created');
            });

            fx.copy('tmp', 'public/export/'+presentation[0].title,{replace: true}, function (err) {
                if (err) return console.error(err);
                console.log('TMP copyed to public/export');
                     var exportzip = new zip.EasyZip();
                    exportzip.zipFolder('public/export/'+presentation[0].title, function(){
                    exportzip.writeToFile('public/export/'+presentation[0].title+'.zip');
                });
            });

            //----export/zip
           

                // get and return the presentation 
                res.json(presentation);
            });
        });

    //----------------END EXPORT-----------------------------


        // route to handle creating goes here (app.post)
        app.post('/api/presentations', function(req, res) {
            // create a presentation, information comes from AJAX request from Angular
            Presentation.createDate = Date.now();

            Presentation.create({
                title       : req.body.title,
                css         : [
                    {
                        "Theme"     : "designnonplus", 
                        "Effect"    : "parallax",  
                        "Alignment" : "alignmentcenter",  
                        "Fontalign" : "fontcenter", 
                        "Colors"    : "", 
                        "Font"      : "",
                        "GoogleFont": ""
                    }
                ],
                autor       : req.body.autor,
                password    : req.body.password,
                sections    : [
                    {
                        id          : 0,
                        subsections : [
                            {
                                id          : 0,
                                css         : [
                                    {
                                        "Background"    : "", 
                                        "Custom"        : {}
                                    }
                                ],
                                elements    : []
                            }
                        ]
                    }
                ]
            }, function(err, presentation) {
                if (err)
                    res.send(err);
                // get and return all the presentations after you create another
                Presentation.find(function(err, presentations) {
                    if (err)
                        res.send(err)
                    res.json(presentations);
                });
            });
        });

        //UPDATE
        app.put('/api/presentations/:presentation_id', function(req, res, next) {
            Presentation.findByIdAndUpdate(req.params.presentation_id, req.body, function(err, post){
                if(err) return next(err);
                res.json(post);
            });
        });
        // route to handle delete goes here (app.delete)
        app.delete('/api/presentations/:presentation_id', function(req, res) {
            Presentation.remove({
                _id : req.params.presentation_id
            }, function(err, presentation) {
                if (err)
                    res.send(err);
                // get and return all the presentations after you create another
                Presentation.find(function(err, presentations) {
                    if (err)
                        res.send(err)
                    res.json(presentations);
                });
            });
        });





        // item routes ===========================================================
 
         app.get('/api/items', function(req, res) {
            Item.find(function(err, items) {
                if (err)
                    res.send(err);

                res.json(items);
            });
        });

        app.get('/api/items/:item_id', function(req, res) {
            Item.find({
                _id : req.params.item_id
            }, function(err, item) {
                if (err)
                    res.send(err);
                res.json(item);
            });
        });

        app.post('/api/items', function(req, res) {
            Item.create({
                title       : req.body.title,
                groups    : [
                    {
                        property          : req.body.property,
                        items : [
                            {
                                item            : req.body.item,
                                value           : req.body.value
                            }
                        ]
                    }
                ]
            }, function(err, item) {
                if (err)
                    res.send(err);
                Item.find(function(err, items) {
                    if (err)
                        res.send(err)
                    res.json(items);
                });
            });
        });





        // frontend routes =========================================================
        // route to handle all angular requests
        app.get('*', function(req, res) {
            res.sendfile('./public/index.html'); // load our public/index.html file
        });

    };
