// app/models/presentation.js
// grab the mongoose module
var mongoose = require('mongoose');

// define our user model
// module.exports allows us to pass this to other files when it is called
module.exports = mongoose.model('Presentation', {
    title       : {type : String, default : "", required : true},
    css       : {type : Array, default : ["designnonplus"]},
    autor 		: {type : String, default : "", required : true},
    date 		: {type : Date, default : Date.now},
    password 	: {type : String, default : ""},
    sections 	: {type : Array, default : [
    {
    	id 			: {type : Number, default : 0},
    	subsections : {type : Array, default : [
    	{
    		id 			: {type : Number, default: 0}, 
            properties  : {type : Array, default : []},
            css         : {type : Array, default : [{"Background":""}]},
    		elements 	: {type : Array, default : [{
                type       : {type : String, default : ""},
                content     : {type : String, default : ""},
                property    : {type : Object, default : {}}
            }]}
    	}]}
    }]}
});



