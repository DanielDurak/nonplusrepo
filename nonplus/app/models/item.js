// app/models/item.js
var mongoose = require('mongoose');

module.exports = mongoose.model('Item', {
    title 		: {type : String, default : "", required : true},
    groups 	: {type : Array, default : [
        {
        	property       : {type : String, default : ""},
        	items    : {type : Array, default : [
            	{
            		item 		: {type : String, default : ""}, 
            		value      	: {type : String, default : ""}
            	}
            ]}
        }
    ]}
});


