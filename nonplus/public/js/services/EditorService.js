// public/js/services/EditorService.js
angular.module('EditorService', []).factory('Presentation', ['$http', function ($http) {

       return {
        // call to get the presentation
        get : function() {
            return $http.get('/api/presentations');
        },
        getItems : function() {
            return $http.get('/api/items');
        },
        put : function(id) {
            return $http.put('/api/presentations'+ id);
        },
                // these will work when more API routes are defined on the Node side of things
        // call to POST and create a new presentation
        create : function(presentationData) {
            return $http.post('/api/presentations', presentationData);
        },

        // call to DELETE a presentation
        delete : function(id) {
            return $http.delete('/api/presentations/' + id);
        }
    }      

}]);

