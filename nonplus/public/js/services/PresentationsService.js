// public/js/services/PresentationsService.js
angular.module('PresentationsService', []).factory('Presentation', ['$http', function ($http) {

    return {
        // call to get all presentations
        get : function() {
            return $http.get('/api/presentations');
        },
                // these will work when more API routes are defined on the Node side of things
        // call to POST and create a new presentation
        create : function(presentationData) {
            return $http.post('/api/presentations', presentationData);
        },

        // call to DELETE a presentation
        delete : function(id) {
            return $http.delete('/api/presentations/' + id);
        }
    }       

}]);

