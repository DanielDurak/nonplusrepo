// public/js/appRoutes.js
    angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

    $routeProvider

        // home page
        .when('/', {
            templateUrl: 'views/home.html',
            controller: 'MainController'
        })

        // user page that will use the UserController
        .when('/user', {
            templateUrl: 'views/user.html',
            controller: 'UserController'
        })

        // presentations page that will use the PresentationsController
        .when('/presentations', {
            templateUrl: 'views/presentations.html',
            controller: 'PresentationsController'
        })

        // editor page that will use the EditorController
        .when('/editor/:id', {
            templateUrl: 'views/editor.html',
            controller: 'EditorController'
        })

        // overview page that will use the OverviewController
        .when('/overview/:id', {
            templateUrl: 'views/overview.html',
            controller: 'OverviewController'
        })

        // preview page that will use the PreviewController
        .when('/preview/:id', {
            templateUrl: 'views/preview.html',
            controller: 'PreviewController'
        })

        // preview page that will use the PreviewController
        .when('/test', {
            templateUrl: 'views/test.html',
            controller: 'TestController'
        });

    $locationProvider.html5Mode(true);

}]);


