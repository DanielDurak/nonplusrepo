
// public/js/controllers/UserCtrl.js
angular.module('UserCtrl', []).controller('UserController', function($scope, $http) {

$scope.buttonActive = 'users';
$scope.formData = {};
$scope.shownUserPres = "";

    // when landing on the page, get all users and show them
    $http.get('/api/users')
        .success(function(data) {
            $scope.users = data;
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });
    $http.get('/api/presentations')
        .success(function(data) {
            $scope.presentations = data;
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });

    // when submitting the add form, send the text to the node API
    $scope.createUser = function() {
        $http.post('/api/users', $scope.formData)
            .success(function(data) {
                $scope.formData = {}; // clear the form so our user is ready to enter another
                $scope.users = data;
                $scope.create = false;
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };
    $scope.updateUser = function() {
        $http.put('/api/users/' + $scope.userid, $scope.formData)
            .success(function(data) {
                $scope.formData = {}; // clear the form so our user is ready to enter another
                //$scope.users = data;
                $scope.back();
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };

    // delete a user after checking it
    $scope.deleteUser = function(id) {
        $http.delete('/api/users/' + id)
            .success(function(data) {
                $scope.users = data;
                $scope.del = false;
                $scope.popupsubHeader = "";
                $scope.popupHeader = "Add a new user !";
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };



    $scope.create = false;
    $scope.del = false;
    $scope.edit = false;
    $scope.reversed=false;
    $scope.popupHeader = "Add a new user !";
    $scope.popupsubHeader = "";
    $scope.order="email";


    $scope.reallyDelete = function(user){
        $scope.del = true;
        $scope.popupHeader = "Do You really want to delete";
        $scope.popupsubHeader = user.firstname;
        $scope.userid = user._id;
    }
    $scope.editUser = function(user){
        $scope.formData = user;
        $scope.edit = true;
        $scope.userid = user._id;
    }
    $scope.back = function (){
        $scope.formData = {};
        $scope.create = false;
        $scope.del = false;
        $scope.edit = false;
        $scope.popupHeader = "Add a new user !";
        $scope.popupsubHeader = "";
    }
});