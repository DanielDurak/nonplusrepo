
// public/js/controllers/EditorCtrl.js
angular.module('OverviewCtrl', []).controller('OverviewController', function($scope, $http, $routeParams, $location, hotkeys) {
	
	$scope.presentationid = $routeParams.id;

	$scope.view = 'overview';
	$scope.viewOverview = true;
	$scope.activeSection = 0
	$scope.activeSubSection = 0;
	$scope.isFullscreen = false;

    $http.get('/api/presentations/' + $scope.presentationid)
        .success(function(data) {
            $scope.presentation = data[0];
			$scope.sections = $scope.presentation.sections;

			document.getElementById('overviewContent').style.width = ($scope.presentation.sections.length*305) +"px";
			var oldSubSectionWidth = 0;
			for(var i = 0; i < $scope.presentation.sections.length; i++){
				if($scope.presentation.sections[i].subsections.length > oldSubSectionWidth){
					oldSubSectionWidth = $scope.presentation.sections[i].subsections.length;
				}
			}
			document.getElementById('overviewContent').style.height = (oldSubSectionWidth*205) +"px";


        })
        .error(function(data) {
            console.log('Error: ' + data);
        });


//---------------------HOTKEYS----------------------
//--------------------------------------------------
  hotkeys.bindTo($scope)
    .add({
      combo: 'p',
      description: 'Preview',
      callback: function() {
        $location.path('/preview/'+$scope.presentationid);
      }
    })
    .add({
      combo: 'e',
      description: 'Editor',
      callback: function() {
        $location.path('/editor/'+$scope.presentationid);
      }
    })
    .add({
      combo: 'f',
      description: 'Fullscreen',
      callback: function() {
        $scope.isFullscreen = !$scope.isFullscreen;
      }
    });
//---------------------HOTKEYS----------------------
//--------------------------------------------------

$scope.dragStart = function (){
  console.log("start");
}
  


});