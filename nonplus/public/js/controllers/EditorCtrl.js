
// public/js/controllers/EditorCtrl.js
angular.module('EditorCtrl', []).controller('EditorController', function($scope, $http, $routeParams, $cookieStore, $location, hotkeys) {

$scope.view = 'editor';
$scope.viewEditor = true;
$scope.activeItem;
$scope.exportDownload = false;
$scope.isFullscreen = false;
$scope.formData = {};
$scope.imageData;
$scope.custombackground;
var scaling = false;
var moving = false;
$scope.tagline = "hier steht der coolste text";
$scope.presentationid = $routeParams.id;


    $http.get('/api/presentations/' + $scope.presentationid)
        .success(function(data) {
            $scope.presentation = data[0];
            $scope.presentationJSON = angular.toJson($scope.presentation, true);
			$scope.sections = $scope.presentation.sections;
			if($scope.presentation.password != ''){
				if($cookieStore.get('currentUser').email==data[0].autor){
					$scope.confirmed=false;
				}
				else{
					$scope.confirmed = true;
					$scope.popupHeader = "The autor left a password !";
				}
			}
			else{
				$scope.confirmed = false;
			}
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });
    $http.get('/api/items/')
        .success(function(data) {
            $scope.items = data;
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });


//---------------------HOTKEYS----------------------
//--------------------------------------------------
  hotkeys.bindTo($scope)
    .add({
      combo: 'f',
      description: 'Toggle Fullscreen',
      callback: function() {
        $scope.isFullscreen = !$scope.isFullscreen;
      }
    })
    .add({
      combo: 'p',
      description: 'Preview',
      callback: function() {
        $location.path('/preview/'+$scope.presentationid);
      }
    })
    .add({
      combo: 'o',
      description: 'Overview',
      callback: function() {
        $location.path('/overview/'+$scope.presentationid);
      }
    })
    .add({
      combo: 'ctrl+s',
      description: 'Save',
      callback: function() {
        $scope.change('','');
        alert("saved");
      }
    });
//---------------------HOTKEYS----------------------
//--------------------------------------------------



//---------------Password-Check--------------------
$scope.check = function (){
		if($scope.formData.password==this.presentation.password){
			$scope.confirmed = false;
		}
		else{
			$scope.confirmed = true;
			$scope.popupHeader = "Wrong Password !";
			$scope.popupsubHeader = "please try again";
			$scope.formData.password = "";
		}
	
}
    
$scope.exportPresentation = function(){
    $http.get('/api/presentations/' + $scope.presentationid + '/export')
        .success(function(data) {
        	$scope.exportDownload = true;
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });
}

$scope.importPresentation = function(){
	$scope.import = true;
	$scope.popupHeader = "Choose a presentation- \".json\"-file !"
}

    $scope.importStart = function(){
        $http.get('img/test.json')
        .success(function(importData) {
            delete importData._id;
            importData.title = $scope.presentation.title;
            importData.password = $scope.presentation.password;
            importData.date = $scope.presentation.date;
            importData.autor = $scope.presentation.autor;
            $scope.presentation = importData;
            $scope.change('','');
            $scope.import = false;
        });
    }
//--------------Navigation------------------------
	$scope.showSubnav = function (itemid) {
		$scope.shownItem = itemid;
	}
	$scope.showValue = function (item) {
		$scope.shownValue = item;
	}
	$scope.hideSubnav = function () {
		$scope.shownItem = '';
	}
	$scope.hideValue = function () {
		$scope.shownValue = '';
	}
	
//--------------ElementITEMS-------------------
	$scope.elementitems = [
		{
			"title":"Text",
			"subitems":["Normal","List","Heading1", "Heading2"]
		},
		{
			"title":"Box",
			"subitems":["Normal","Rounded"]
		},
		{
			"title":"Table",
			"subitems":["2x2","2x3","3x2", "3x3"]
		},
		{
			"title":"Image",
			"subitems":["Upload","URL"]
		},
		{
			"title":"Del",
			"subitems":["Element","Page"]
		}
	];

	$scope.dragSuccess = function(title, subtitle, event){

		var object = {};
		var content = "";
		var type = "";
		var properties = {};
		var newElementId = $scope.presentation.sections[$scope.activeSection].subsections[$scope.activeSubSection].elements.length;

		if(title=='Del'){
			if(subtitle=='Element'){
				$scope.delElement();
			}
			else{
				$scope.deleteSubSection();
			}
		}
		else{

			if(title=='Text'){
				if(subtitle=='Normal'){
					content = "<p>Normal text</p>";
					type = "text";
					properties = {"width":"75%"}
				}
				if(subtitle=='List'){
					content = "<ul><li>Item 1</li><li>Item 2</li><li>Item 3</li></ul>";
					type = "list";
					properties = {"width":"15%","text-align":"left"}
				}
				if(subtitle=='Heading1'){
					content = "<h1>Heading 1</h1>";
					type = "text";
					properties = {"width":"75%"};
				}
				if(subtitle=='Heading2'){
					content = "<h2>Heading 2</h2>";
					type = "text";
					properties = {"width":"75%"};
				}
			}
			if(title=='Box'){
				content = "";
				type = "box";
				if(subtitle=='Normal'){
					properties = {"width":"75%", "background-color":"rgba(100,0,0,0.5)", "height":"100px"};
				}
				if(subtitle=='Rounded'){
					properties = {"width":"75%", "background-color":"rgba(100,0,0,0.5)", "height":"100px", "border-radius":"20px"};
				}
			}
			if(title=='Table'){
				type = "table";
				properties = {"width":"75%", "background-color":"rgba(0,0,0,0.1)"};
				if(subtitle=='2x2'){
					content = "<table><tr><th>Head1</th><th>Head2</th></tr><tr><td>Data1</td><td>Data2</td></tr></table>";
				}
				if(subtitle=='2x3'){
					content = "<table><tr><th>Head1</th><th>Head2</th></tr><tr><td>Data1</td><td>Data2</td></tr><tr><td>Data1</td><td>Data2</td></tr></table>";
				}
				if(subtitle=='3x2'){
					content = "<table><tr><th>Head1</th><th>Head2</th><th>Head3</th></tr><tr><td>Data1</td><td>Data2</td><td>Data3</td></tr></table>";
				}
				if(subtitle=='3x3'){
					content = "<table><tr><th>Head1</th><th>Head2</th><th>Head3</th></tr><tr><td>Data1</td><td>Data2</td><td>Data3</td></tr><tr><td>Data1</td><td>Data2</td><td>Data3</td></tr></table>";
				}
			}
			if(title=='Image'){
				type = "image";
				properties = {"width":"20%", "line-height":"1px"};

					if($scope.image){
						if(subtitle!="Upload"){
							content = "<img src=\""+$scope.imageData+"\" width=\"100%\">";
							console.log("!upload");
						}
						else{
							content = "<img src=\"img/"+event+"\" width=\"100%\">";
							console.log("upload");
						}
						$scope.image=false;

					}
					else{
						$scope.uploadPopup(subtitle);
					}
			}

			object = {"type":type,"id":newElementId,"content":content,"properties":properties};
			
			//console.log(object);
			if(content!=""&&type!="box"){
				$scope.addElement(object);
			}

		}



	}

	//----------------ADD-ELEMENTS-------------
	$scope.addElement = function(obj){
		$scope.presentation.sections[$scope.activeSection].subsections[$scope.activeSubSection].elements.push(obj);
		$scope.change('','');
	}

	//----------------DELETE-ELEMENTS-------------
	$scope.delElement = function(){
		$scope.presentation.sections[$scope.activeSection].subsections[$scope.activeSubSection].elements.splice($scope.chosenElement, 1);
		$scope.change('','');
		//Index von nachfolgenden Elementen müssen angepasst werden !
	}
	//----------------DELETE-SubSections-------------
	$scope.delSubSec = function(){
		$scope.presentation.sections[$scope.activeSection].subsections.splice($scope.activeSubSection, 1);
		//$scope.change('','');
		//Index von nachfolgenden SubSecs müssen angepasst werden !
	}


	//----------------SCALE-ELEMENTS-----------
	$scope.scaleStart = function (e){
		scaling=true;
		$scope.editorWidth = document.getElementsByClassName('editor')[0].clientWidth;
		$scope.editorOffsetX = document.getElementsByClassName('editor')[0].offsetLeft;
		var editorRight = $scope.editorOffsetX + $scope.editorWidth;
	}
	$scope.scaleMove = function (e){
		if(scaling){
			//Maus-Position in Pixel
			var mouseWidthPx = e.x - $scope.editorOffsetX - e.srcElement.parentElement.parentElement.offsetLeft;
			var mouseWidthPr = ((mouseWidthPx + 2) / $scope.editorWidth) * 100;
			if(mouseWidthPr>100){
				var newWidth = "100%";
			}
			else{
				var newWidth = Math.round(mouseWidthPr)+"%";
			}
			$scope.presentation.sections[$scope.activeSection].subsections[$scope.activeSubSection].elements[e.srcElement.parentElement.parentElement.id].properties.width = newWidth;
		}
	}
	$scope.skaleEnd = function (){
		scaling=false;
		$scope.change("","");
	}

	//----------------MOVE-ELEMENTS-----------

//--------------ElementChoosing-------------------
$scope.init = function(){
	$scope.mediumeditor = new MediumEditor('.editable');
}

$scope.chooseElement = function(section, subsection, element){
	$scope.chosenElement=element.id;
	console.log($scope.chosenElement);
	var elIndex = 0;
	for (var secCount = 0; secCount< $scope.presentation.sections.length; secCount++) {
		for (var subsecCount = 0; subsecCount < $scope.presentation.sections[secCount].subsections.length; subsecCount++) {
			for (var elCount = 0; elCount < $scope.presentation.sections[secCount].subsections[subsecCount].elements.length; elCount++) {
				elIndex++;
				if(secCount==section.id&&subsecCount==subsection.id&&elCount==element.id){
					$scope.presentation.sections[section.id].subsections[subsection.id].elements[element.id].content = $scope.mediumeditor.elements[elIndex-1].innerHTML;
					$scope.change('','');
				}
			};
		};
	};
}

//--------------Customize------------------------

$scope.customize = function(){
	$scope.custom = true;
    $scope.popupHeader = "Customize your presentation !";
}
$scope.saveCustom = function(json){
	$scope.custom = false;
    $scope.presentation = JSON.parse(json);
    $scope.change('','');
}
$scope.bgpopup = function(){
	$scope.backgroundcolor=true;
	$scope.popupHeader = "Create a new background";
	$scope.activeBackgroundcolorRendered = {"background": $scope.presentation.sections[$scope.activeSection].subsections[$scope.activeSubSection].css[0].Custom.background};
}
$scope.fontpopup = function(){
	$scope.googlefont=true;
	$scope.popupHeader = "Insert the Name of your GoogleFont";
	$scope.activeGooglefont = "Amatic SC";

}
	$scope.changeCustom = function(item, value){
        if(item=="background"){
        	var customObject = $scope.presentation.sections[$scope.activeSection].subsections[$scope.activeSubSection].css[0].Custom;
        	var existingBg = false;
        	angular.forEach(customObject, function(objectValue, key){
        		if(key=="background"){
        			$scope.presentation.sections[$scope.activeSection].subsections[$scope.activeSubSection].css[0].Custom.background = value.background;
        			existingBg = true;
        		}
        	});
        	if(!existingBg){
				$scope.presentation.sections[$scope.activeSection].subsections[$scope.activeSubSection].css[0].Custom.push({"background" : value});
        	}
        
        }
        if(item=="googlefont"){
        	if(value)
        	$scope.presentation.css[0].GoogleFont = value;
        	$scope.presentation.css[0].Font = "";
        }
		$scope.change('','');
		$scope.back();
	}
	$scope.renderColor = function (bgcolor, bgcolor2) {
		var bgcolorrendered = {};
		if(bgcolor2==null){
			bgcolorrendered = {"background": bgcolor};
		}
		else{
			bgcolorrendered =  {
				"background" : "radial-gradient(ellipse at center, "+bgcolor+" 0%,"+bgcolor2+" 100%)"
			};
		}

		$scope.activeBackgroundcolorRendered = bgcolorrendered;
	}
$scope.uploadPopup = function(type){
	$scope.image = true;
	if(type=="URL"){
		$scope.uploadUrl=true;
    	$scope.popupHeader = "Upload an image from the World Wide Web";
	}
	else{
    	$scope.popupHeader = "Upload an image from your PC";
		$scope.uploadUrl=false;
	}
}



$scope.back = function (){
    $scope.formData = {};
    $scope.custom = false;
    $scope.image = false;
    $scope.backgroundcolor=false;
    $scope.googlefont = false;
    $scope.import = false;
}


//--------------Pagination------------------------

	if($scope.activeSection==null && $scope.activeSubSection==null){
		$scope.activeSection=0;
		$scope.activeSubSection=0;
		$scope.chosenElement = -1;
	}

	$scope.chooseSubSection = function(secid, subsecid){
		$scope.activeSection=secid;
		$scope.activeSubSection=subsecid;
	}

//--------------Changing------------------------
	$scope.change = function (item, property) {
		if(item.property == 'Theme'){
			//ABRAGE ob wirklich sicher ? da css einzelner subsections entfernt werden
			for (var secCount = 0; secCount< $scope.presentation.sections.length; secCount++) {
				for (var subsecCount = 0; subsecCount < $scope.presentation.sections[secCount].subsections.length; subsecCount++) {
					$scope.presentation.sections[secCount].subsections[subsecCount].css[0] = {"Background":"","Custom":{}};
				};
			};
			$scope.presentation.css[0].Theme = property.value;
		}
		if(item.property == 'Colors'){
			$scope.presentation.css[0].Colors = property.value;
		}	
		if(item.property == 'Background'){

			if(property.value=='backgroundcolorcustom'){
				$scope.bgpopup();
			}
			else{
				$scope.presentation.sections[$scope.activeSection].subsections[$scope.activeSubSection].css[0].Custom.background = "";
				$scope.presentation.sections[$scope.activeSection].subsections[$scope.activeSubSection].css[0].Background = property.value;
			}

		}	
		if(item.property == 'Typography'){
			if(property.value=='fontgooglefont'){
				$scope.fontpopup();
			}
			else{
				$scope.presentation.css[0].GoogleFont = "";
				$scope.presentation.css[0].Font = property.value;
			}
		}	
		if(item.property == 'Page-Effect'){
			$scope.presentation.css[0].Effect = property.value;
		}	
		if(item.property == 'Font-Effect'){
			$scope.presentation.css[0].Fragments = property.value;
		}	
		if(item.property == 'Element Alignment'){
			$scope.presentation.css[0].Alignment = property.value;
		}	
		if(item.property == 'Font-Position'){
			$scope.presentation.css[0].Fontalign = property.value;
		}	
		
		$scope.formData = $scope.presentation;
        $http.put('/api/presentations/' + $scope.presentationid, $scope.formData)
            .success(function(data) {
                $scope.formData = {}; 
            })
            .error(function(data) {
                console.log('Error: ' + data);
           	});	
	}



//-----------------ADD-SUBSECTION------------------
	$scope.addSubSection = function () {
		var subsectionAmount = $scope.presentation.sections[$scope.activeSection].subsections.length;
		var followingSubsections = subsectionAmount-($scope.activeSubSection+1);
		//Wenn Anzahl kleiner oder gleich der aktuellen Subsection, dann push seubsection an Ende
		if($scope.activeSubSection+1==subsectionAmount){
			$scope.presentation.sections[$scope.activeSection].subsections.push(
				{
					id: $scope.activeSubSection+1,
					css: [{"Background" : "", "Custom" : {"background":""}}],
					elements: []
				}
			);
		}
		else{
			//Hilfs-Array in welchem folgende Objekte gespeichert werden für Dreieckstausch
			var helperSubsections = [];
			//For-Schleife, in welcher alle folgenden Objekte in Hilfs-Array gespeichert
			for(followingSubsections; followingSubsections>0; followingSubsections--){
				helperSubsections.push($scope.presentation.sections[$scope.activeSection].subsections[$scope.activeSubSection+1]);
				//Löschen aller nachfolgenden Subsections aus $scope
				$scope.presentation.sections[$scope.activeSection].subsections.splice($scope.activeSubSection+1,1);
			}
			//Hinzufügen der neuen "ZwischenSubsection"
			$scope.presentation.sections[$scope.activeSection].subsections.push(
				{
					id: $scope.activeSubSection+1,
					css: [{"Background" : "", "Custom" : {"background":""}}],
					elements: []
				}
			);
			//Aufnahme alter Subsection-Objekte aus Hilfs-Array zurück in $scope
			for(var counter = 1; counter <= helperSubsections.length; counter++){
				$scope.presentation.sections[$scope.activeSection].subsections.push(
					{
						id: $scope.activeSubSection+(counter+1),
						css: helperSubsections[counter-1].css,
						elements: helperSubsections[counter-1].elements
					}
				);
			}
		}
		//Aktualisieren der AktivSektion 
		$scope.activeSubSection+=1;
		$scope.change('','');
	}

//-----------------ADD-SECTION----------------------
	$scope.addSection = function () {
		var sectionAmount = $scope.sections.length;
		var followingSections = sectionAmount-($scope.activeSection+1);
		//Wenn Anzahl kleiner oder gleich der aktuellen Section, dann push ssection an Ende
		if($scope.activeSection+1==sectionAmount){
			$scope.sections.push(
				{
					id: $scope.activeSection+1,
					subsections: [
						{
							id: 0,
							css : [{"Background":"", "Custom" : {"background":""}}],
							elements : []
						}
					]
				}
			);
		}
		else{
			//Hilfs-Array in welchem folgende Objekte gespeichert werden für Dreieckstausch
			var helperSections = [];
			//For-Schleife, in welcher alle folgenden Objekte in Hilfs-Array gespeichert
			for(followingSections; followingSections>0; followingSections--){
				helperSections.push($scope.presentation.sections[$scope.activeSection+1]);
				//Löschen aller nachfolgenden Sections aus $scope
				$scope.presentation.sections.splice($scope.activeSection+1,1);
			}
			//Hinzufügen der neuen "ZwischenSection"
			$scope.sections.push(
				{
					id: $scope.activeSection+1,
					subsections: [
						{
							id: 0,
							css : [{"Background":"", "Custom" : {}}],
							elements : []
						}
					]
				}
			);
			//Aufnahme alter Section-Objekte aus Hilfs-Array zurück in $scope
			for(var counter = 1; counter <= helperSections.length; counter++){
				$scope.presentation.sections.push(helperSections[counter-1]);
				$scope.presentation.sections[$scope.activeSection+(counter+1)].id++;
			}
		}
		//Aktualisieren der AktivSektion und Anpassung der Overviewbreite
		$scope.activeSection+=1;
		$scope.activeSubSection=0;
		$scope.change('','');
	}



//-----------------Delete-SUBSECTION------------------
	$scope.deleteSubSection = function () {
		var subsectionAmount = $scope.presentation.sections[$scope.activeSection].subsections.length;
		var followingSubsections = subsectionAmount-($scope.activeSubSection+1);
		//Wenn Anzahl kleiner oder gleich der aktuellen Subsection, dann slice seubsection
		if($scope.activeSubSection+1==subsectionAmount){
				$scope.presentation.sections[$scope.activeSection].subsections.splice($scope.activeSubSection,1);
				$scope.activeSubSection--;
		}
		else{
			//Hilfs-Array in welchem folgende Objekte gespeichert werden für Dreieckstausch
			var helperSubsections = [];
			//For-Schleife, in welcher alle folgenden Objekte in Hilfs-Array gespeichert
			for(followingSubsections; followingSubsections>0; followingSubsections--){
				helperSubsections.push($scope.presentation.sections[$scope.activeSection].subsections[$scope.activeSubSection+1]);
				//Löschen aller nachfolgenden Subsections aus $scope
				$scope.presentation.sections[$scope.activeSection].subsections.splice($scope.activeSubSection+1,1);
			}
			//Aufnahme alter Subsection-Objekte aus Hilfs-Array zurück in $scope
			for(var counter = 1; counter <= helperSubsections.length; counter++){
				$scope.presentation.sections[$scope.activeSection].subsections.push(
					{
						id: $scope.activeSubSection+(counter-1),
						elements: helperSubsections[counter-1].elements,
						css: helperSubsections[counter-1].css
					}
				);
			}
		}
		//Function "wenn letzte subsection einer section, dann lösche section und oder nachfolgende" fehlt
		$scope.change('','');
	}


})
.config(['flowFactoryProvider', function (flowFactoryProvider) {
    flowFactoryProvider.defaults = {
      target: '/uploads',
      permanentErrors: [500, 501],
      maxChunkRetries: 5,
      chunkRetryInterval: 5000,
      simultaneousUploads: 1
    };
    flowFactoryProvider.on('catchAll', function (event) {
      console.log('catchAll', arguments);
    });
    // Can be used with different implementations of Flow.js
    //flowFactoryProvider.factory = fustyFlowFactory;
  }]);