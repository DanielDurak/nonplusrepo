// public/js/controllers/TestCtrl.js
angular.module('TestCtrl', []).controller('TestController', function($scope, $http, $cookieStore) {

	$scope.sections = [
		{
			"subsections":[
				{"title":"1"},
				{"title":"2"},
				{"title":"3"},
				{"title":"4"}
			]
		},
		{
			"subsections":[
				{"title":"5"},
				{"title":"6"},
				{"title":"7"}
			]
		},
		{
			"subsections":[
				{"title":"8"},
				{"title":"9"},
				{"title":"10"},
				{"title":"11"}
			]
		}
	];
});

