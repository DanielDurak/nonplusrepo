// public/js/controllers/MainCtrl.js
angular.module('MainCtrl', []).controller('MainController', function($scope, $http, $cookieStore) {
$scope.login=true;
$scope.formData = {};
$scope.usercreate = false;
    // when submitting the add form, send the text to the node API

    $http.get('/api/users')
        .success(function(data) {
            $scope.users = data;
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });

    $scope.createUserCheck = function() {

		var createStat = true;

    	if($scope.formData.firstname != null && $scope.formData.lastname != null && $scope.formData.email != null && $scope.formData.password != null){

	    	if($scope.formData.password == $scope.formData.passwordrep){
		   		$scope.users.forEach(function(existingUser, index){
		   			if(existingUser.email==$scope.formData.email){
		   				console.log(existingUser.email+" : "+$scope.formData.email);
		   				$scope.userCreate = true;
		   				$scope.popupsubHeader = "Email already exists !";
	        			$scope.formData.email = "";
		   				createStat = false;
		   			}
		   		});
		   	}
		   	else{
		   		createStat = false;
		   		$scope.userCreate = true;
		   		$scope.popupsubHeader = "The password repetition is incorrect !";
	        	$scope.formData.password = "";
	        	$scope.formData.passwordrep = "";
		   	}
    	}
    	else{
		   	createStat = false;
    		$scope.userCreate = true;
		   	$scope.popupsubHeader = "Please make sure everything is filled in !";
	   	}

	   	if(createStat){
	   		$scope.createUser();
	   	}

    }

    $scope.back = function (){
        $scope.userCreate = false;
    }

    $scope.createUser = function() {
        $http.post('/api/users', $scope.formData)
            .success(function(data) {
                $scope.formData = {}; 
                $cookieStore.put('currentUser', $scope.formData);
            	window.location.href = '/presentations';
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };
    $scope.loginUser = function() {

		var loginStat = false;
		var existingEmail = false;

    	if($scope.formData.email != null && $scope.formData.email != ""){
    		if($scope.formData.password != null && $scope.formData.password != ""){
				$scope.users.forEach(function(existingUser, index){
		   			if(existingUser.email==$scope.formData.email){
			   			existingEmail = true;
			   			if(existingEmail){
							if(existingUser.password==$scope.formData.password){
								loginStat = true;
	                			$cookieStore.put('currentUser', existingUser);
	                			console.log(existingUser);
							}	
						}
						else{
				   			loginStat = false;
		    				$scope.userCreate = true;
				   			$scope.popupsubHeader = "This Email doesn't exist !";
						}
		   			}
		   		});
		   		if(existingEmail&&!loginStat){
    				$scope.userCreate = true;
		   			$scope.popupsubHeader = "Wrong Password !";
		   		}

       		}
       		else{
			   	loginStat = false;
	    		$scope.userCreate = true;
			   	$scope.popupsubHeader = "Please set a Password";
       		}
    	}
    	else{
		   	loginStat = false;
    		$scope.userCreate = true;
		   	$scope.popupsubHeader = "Please set an Email";
	   	}
	   	if(loginStat){window.location.href = '/presentations';}

    };
});

