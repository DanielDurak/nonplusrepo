
// public/js/controllers/PreviewCtrl.js
angular.module('PreviewCtrl', []).controller('PreviewController', function($scope, $http, $routeParams, $location, hotkeys) {

	$scope.presentationid = $routeParams.id;
    
    $scope.view = 'preview';
    $scope.viewPreview = true;
    $scope.activeSection = 0
    $scope.activeSubSection = 0;
    $scope.activeElement=0;
    $scope.isFullscreen = false;  
    $scope.blackScreen = false;    
    $http.get('/api/presentations/' + $scope.presentationid)
        .success(function(data) {
            $scope.presentation = data[0];
			$scope.sections = $scope.presentation.sections;
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });


//---------------------HOTKEYS----------------------
//--------------------------------------------------
  hotkeys.bindTo($scope)
    .add({
      combo: 'f',
      description: 'Toggle Fullscreen',
      callback: function() {
        $scope.isFullscreen = !$scope.isFullscreen;
      }
    })
    .add({
      combo: 'e',
      description: 'Editor',
      callback: function() {
        $location.path('/editor/'+$scope.presentationid);
      }
    })
    .add({
      combo: 'o',
      description: 'Overview',
      callback: function() {
        $location.path('/overview/'+$scope.presentationid);
      }
    })
    .add({
      combo: 'b',
      description: 'Blackscreen',
      callback: function() {
        $scope.blackScreen = !$scope.blackScreen;
        console.log( $scope.blackScreen);
      }
    })
    .add({
      combo: 'down',
      description: 'Next Subsection',
      callback: function() {
        $scope.chooseSubSection($scope.activeSection, $scope.activeSubSection);
      }
    })
    .add({
      combo: '1',
      description: 'Subsection 1',
      callback: function() {
        $scope.activeSubSection = 0;
      }
    })
    .add({
      combo: '2',
      description: 'Subsection 2',
      callback: function() {
        $scope.activeSubSection = 1;
      }
    })
    .add({
      combo: '3',
      description: 'Subsection 3',
      callback: function() {
        $scope.activeSubSection = 2;
      }
    })
    .add({
      combo: '4',
      description: 'Subsection 4',
      callback: function() {
        $scope.activeSubSection = 3;
      }
    })
    .add({
      combo: 'shift+1',
      description: 'Section 1',
      callback: function() {
        $scope.activeSection = 0;
      }
    })
    .add({
      combo: 'shift+2',
      description: 'Section 2',
      callback: function() {
        $scope.activeSection = 1;
      }
    })
    .add({
      combo: 'shift+3',
      description: 'Section 3',
      callback: function() {
        $scope.activeSection = 2;
      }
    })
    .add({
      combo: 'shift+4',
      description: 'Section 4',
      callback: function() {
        $scope.activeSection = 3;
      }
    });
//---------------------HOTKEYS----------------------
//--------------------------------------------------


    $scope.toggleFullscreen = function (){
        $scope.isFullscreen = !$scope.isFullscreen;
    }

//------------PAGINATION-ELEMENTING----------
        $scope.chooseSubSection = function (secid, subsecid){
            var elements = $scope.sections[$scope.activeSection].subsections[$scope.activeSubSection].elements;
            var end = false;

            if(elements.length>$scope.activeElement){
            //------------Element showing-----
                if(elements[$scope.activeElement].type=='fragment'){
                    //wenn nächstes Element == Fragment, dann activeElement++
                    $scope.activeElement++;
                }
                else{
                    //Andernfalls, activeElement++ und rekursiver Methodenaufruf von chooseSubSection(secid, subsecid);
                    $scope.activeElement++;
                    $scope.chooseSubSection($scope.activeSection, $scope.activeSubSection);
                }
            }
            else{
            //------------Pagination---
                //Subsection-switch innerhalb einer Section
                if(subsecid+1<$scope.presentation.sections[$scope.activeSection].subsections.length){
                    $scope.activeSection=secid;
                    $scope.activeSubSection=(subsecid+1);
                    $scope.activeElement=0;
                }
                //Subsection-switch nach nächster Section
                else{
                    if(secid+1<$scope.presentation.sections.length){
                        $scope.activeSection=secid+1;
                        $scope.activeSubSection=0;  
                        $scope.activeElement=0; 
                    }
                    else{
                        console.log("ENDE");
                        end = true;
                        $scope.blackScreen=true;
                    }
                }
                if(!end){
                    $scope.activeElement = 0;
                }
            }
        }

//-------------------------

});