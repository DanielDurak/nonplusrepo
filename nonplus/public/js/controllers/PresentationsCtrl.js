
// public/js/controllers/PresentationsCtrl.js
angular.module('PresentationsCtrl', []).controller('PresentationsController', function($scope, $http, $cookieStore) {

	
$scope.buttonActive = 'presentations';
$scope.formData = {};
$scope.formData.password="";
    // when landing on the page, get all presentations and show them
    $http.get('/api/presentations')
        .success(function(data) {
            $scope.presentations = data;
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });
    // when submitting the add form, send the text to the node API
    $scope.createStart = function (){
        $scope.create = true;
        $scope.formData.autor = $cookieStore.get('currentUser').email;
    }
    $scope.createPresentation = function() {
        $http.post('/api/presentations', $scope.formData)
            .success(function(dataPres) {
                $scope.formData = {}; // clear the form so our user is ready to enter another
                $scope.presentations = dataPres;
                $scope.create = false;
                angular.forEach(dataPres, function(value, key){

                    if(value.autor == $cookieStore.get('currentUser').email){

                        $http.get('/api/users/'+$cookieStore.get('currentUser')._id)
                            .success(function(dataGetUser) {
                                dataGetUser[0].presentations.push({"id": value._id, "title": value.title});
                                $http.put('/api/users/' + $cookieStore.get('currentUser')._id, dataGetUser[0])
                                    .success(function(dataPutUser) {
                                    })
                                    .error(function(data) {
                                        console.log('Error: ' + dataPutUser);
                                    });
                            })
                            .error(function(dataGetUser) {
                                console.log('Error: ' + dataGetUser);
                        });
                        
                    }



                });
            })
            .error(function(dataPres) {
                console.log('Error: ' + dataPres);
            });
    };


    // delete the presentation after checking it
    $scope.deletePresentation = function(id) {
        if($scope.formData.password==null){
            $scope.formData.password="";
        }
        if($scope.formData.password==$scope.pw){
            $http.delete('/api/presentations/' + id)
                .success(function(dataPres) {

                    $scope.presentations = dataPres;
                    $scope.del = false;
                    $scope.popupsubHeader = "";
                    $scope.popupHeader = "Create a new presentation !";
                    $scope.formData = {};
                })
                .error(function(dataPres) {
                    console.log('Error: ' + dataPres);
                });
        }
        else{
            if($scope.formData.password==''){
                $scope.popupHeader = "A password is needed !";
                $scope.popupsubHeader = "";
            }
            else{
                $scope.popupHeader = "Wrong Password !";
                $scope.popupsubHeader = "please try again";
                $scope.formData.password = "";
            }   
        }
    };



    $scope.create = false;
    $scope.render=false;
    $scope.del = false;
    $scope.activePresentation = '';
    var presentationTitleOld = '';
    $scope.reversed=false;
    $scope.popupHeader = "Create a new presentation !";
    $scope.popupsubHeader = "";
    $scope.order="date";
    
    $scope.showPreview = function (title){
        if(title!=presentationTitleOld){
            $scope.activePresentation = title;
            $scope.render = true;
            presentationTitleOld = title;
        }
        else{
            $scope.closeAll();
            $scope.render = false;
            presentationTitleOld = '';
        }
    }
    $scope.closeAll = function (){
        $scope.activePresentation = '';
    }
    $scope.reallyDelete = function(presentation){
        $scope.del = true;
        $scope.popupHeader = "Do You really want to delete";
        $scope.popupsubHeader = presentation.title;
        $scope.pw = presentation.password;
        $scope.presentationid = presentation._id;
    }
    $scope.back = function (){
        $scope.formData = {};
        $scope.validating = false;
        $scope.create = false;
        $scope.del = false;
        $scope.activePresentation = '';
        $scope.popupHeader = "Create a new presentation !";
        $scope.popupsubHeader = "";
    }


});