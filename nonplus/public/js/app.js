// public/js/app.js

angular.module('nonplusApp', [
'ngRoute', 
'ngSanitize',
'appRoutes', 
'dndLists',
'ngDraggable',
'colorpicker.module',
'ngCookies',
'flow',
'ui.sortable',
'cfp.hotkeys',
'FBAngular',
'MainCtrl', 
'UserCtrl', 
'EditorCtrl', 
'OverviewCtrl', 
'PreviewCtrl', 
'PresentationsCtrl', 
'TestCtrl', 
'UserService',
'PresentationsService',
'EditorService'
]);

