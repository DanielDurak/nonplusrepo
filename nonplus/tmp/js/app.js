angular.module('PresentationApp',[]).controller('PrevCtrl', function($scope){
		
//--------------Pagination------------------------

	if($scope.activeSection==null && $scope.activeSubSection==null){
		$scope.activeSection=0;
		$scope.activeSubSection=0;
		$scope.activeElement = 10;
	}


	$scope.chooseSubSection = function(secid, subsecid){

		var numberOfSubSections = [];
		//Anzahl aller Sections, Subsections z.B. [2, 3] (Sec0: 2*SubSec, Sec1 : 3*SubSec)

		for (var sectionCount = 0; sectionCount < document.getElementsByClassName("section").length; sectionCount++) {
			numberOfSubSections.push(document.getElementsByClassName("section")[sectionCount].getElementsByClassName("subsection").length);
			//Subsections jeweiliger Section
		};

	//------------Pagination---
		//Subsection-switch innerhalb einer Section
		if(subsecid+1<numberOfSubSections[secid]){
			$scope.activeSection=secid;
			$scope.activeSubSection=(subsecid+1);
		}
		//Subsection-switch nach nächster Section
		else{
			if(secid+1<numberOfSubSections.length){
				$scope.activeSection=secid+1;
				$scope.activeSubSection=0;
			}
			//Do - nothing bei Ende
			else{
				console.log("ENDE");
			}
		}
	//-------------------------

	}






});	